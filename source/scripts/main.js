var slideIndex = 0;
var slidetimeout;
var section_list = ["section_1", "section_2", "section_3", "section_5"];
var buttons = ["button1", "button2", "button3", "button4"];


window.scrollBy(0, 2);
window.onscroll = function() {scrollFunction()};

navhighlight();

var modals = document.getElementsByClassName("modal");
var closes = document.getElementsByClassName("close");
var wechat = document.getElementById("wechat");


$('#xn').css('left', ($(window).width() * 0.18) + 'px');
$('#cb').css('right', ($(window).width() * 0.22) + 'px');
$('#book').css({'height': 1.186 * $('#book').width() +'px'});
$('#section_5').css({'height': 1.55 * $('#book').width() +'px'});
$('.webWrapper').css({'height': 1.3 * $('.webWrapper').width() +'px'});



for (var i = 0; i < modals.length; i++) {
  (function (n) {
    document.getElementById("modal"+n).addEventListener("click", function() {
      event.stopPropagation();
      document.getElementById("content" + n).style.display="block";});
  }) (i);
}

for (var i = 0; i < modals.length; i++) {
  (function (n) {
    document.getElementById("close_button"+n).addEventListener("click", function() {
      event.stopPropagation();
      document.getElementById("content" + n).style.display="none";});
  }) (i);
}


var prevScrollTop = 0;
var scrolling = false;
var current = null;
var lastTimeStamp = 0;
$('#scrollable').on('DOMMouseScroll mousewheel', function(e) {
  e.preventDefault();
  var queue = jQuery.queue( $('#scrollable'));
  var lists = ["#gitapp", "#rideshare", "#web"];
  if ($(window).scrollTop() < $('#scrollable').offset().top) {
    window.scrollBy(0, -e.originalEvent.wheelDeltaY);
  } else {
    if (!current || current < 0) current = 0;
    if (current != 2 && !scrolling && e.originalEvent.wheelDeltaY < 0) {
      if (e.timeStamp - lastTimeStamp < 1100) {
        scrolling = false;
        return;
      } else {
        lastTimeStamp = e.timeStamp;
      }
      scrolling = true;
      current += 1;
      var scrollY = $(lists[current]).offset().top - $(window).scrollTop();
      for (var i = 0; i < current - 1; i++) {
        scrollY += $(lists[i]).height();
      }
      if (current == 2) scrollY += 200;
      $('#scrollable').animate({
        scrollTop: scrollY
      }, 480, function(){
        scrolling = false;
      });
      if (current == 2) {
        $('#web1').animate({
          "marginLeft": "+=" + (0.13 * $(window).width()) + "px",
          "marginRight": "+=" + (0.13 * $(window).width()) + "px",
        }, 1800);
        $('#web0').animate({  textIndent: -10}, {
          step: function(now,fx) {
          $(this).css('-webkit-transform','rotate('+now+'deg)');
        }, duration: 1800},'linear');
        $('#web2').animate({  textIndent: 10}, {
          step: function(now,fx) {
          $(this).css('-webkit-transform','rotate('+now+'deg)');
        }, duration: 1800},'linear');
      }
    } else if (!scrolling && e.originalEvent.wheelDeltaY > 0) {
      if (current <= 0) {
        window.scrollBy(0, -e.originalEvent.wheelDeltaY);
      } else {
        if (e.timeStamp - lastTimeStamp < 1100) {
          return;
        } else {
          lastTimeStamp = e.timeStamp;
        }
        scrolling = true;
        current -= 1;
        var scrollY = 0;
        for (var i = 0; i < current; i++) {
          scrollY += $(lists[i]).height();
        }
        $('#scrollable').animate({
          scrollTop: scrollY
        }, 480, function(){
          $('#web1').css("margin", "0px -20%");
          $('#web0').css('-webkit-transform','rotate(-1)');
          $('#web2').css('-webkit-transform','rotate(1)');
        });
      }
      scrolling = false;
    } else if (!scrolling){
      scrolling = true;
      window.scrollBy(0, -e.originalEvent.wheelDeltaY);
      scrolling = false;
    }
  }
});



function navhighlight() {
  for (var i = 0; i < buttons.length; i++) {
    (function (n) {
      document.getElementById(buttons[n]).addEventListener("click", function() {highlight(n);});
    }) (i);
  }
}

function highlight(n) {
  var target = Math.max(0, document.getElementById(section_list[n]).offsetTop - document.getElementById("navbar").offsetHeight);
  var cur = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
  var prev = cur;
  var step = 25;
  if (target < cur)
    step = -25;
  var t = window.setInterval(function(){
    var target = Math.max(0, document.getElementById(section_list[n]).offsetTop);
    var curY = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
    if ((step > 0 && target > curY) || (step < 0 && target < curY)){
      window.scrollBy(0, step);
    }else {
      clearInterval(t);
    }
    var curY = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
    if (prev == curY)
      clearInterval(t);
    if (Math.abs(step) > Math.abs(target - curY)){
      window.scrollTo(0, target);
      clearInterval(t);
    }
    prev = curY;
  }, 10);
}


function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();
    return (docViewBottom >= elemTop);
}

function isScrolledOutOfView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();
    return (docViewBottom >= elemBottom + 280);
}

var bgLoaded = false;
var divTwoLoaded = false;
var lastY = 0;
var loading = false;
function scrollFunction() {
  var curY = $(window).scrollTop();
  if (!loading && bgLoaded && isScrolledOutOfView('#section_1_div1')) {
    loading = true;
    bgLoaded = false;
    let sectionOne = document.getElementById('section_1_div1');
    let sectionTwo = document.getElementById('section_2_div1');
    let z = 1;
    let zz = 0;
    sectionOne.style.opacity = z;
    let t = setInterval(()=> {
      z -= 0.1;
      zz += 0.1;
      sectionOne.style.opacity = z;
      sectionTwo.style.opacity = zz;
      if (z <= 0 && zz >= 1) {
        clearInterval(t);
        loading = false;
      }
    }, 150);
  }
  if (!loading && !bgLoaded && isScrolledIntoView('#section_1_div1') && !isScrolledOutOfView('#section_1_div1')) {
    loading = true;
    bgLoaded = true;
    let sectionOne = document.getElementById('section_1_div1');
    let sectionTwo = document.getElementById('section_2_div1');
    let z = -0.4;
    let zz = 1;
    sectionOne.style.opacity = z;
    let t = setInterval(()=> {
      z += 0.1;
      zz -= 0.1;
      sectionOne.style.opacity = z;
      sectionTwo.style.opacity = zz;
      if (z >= 1 && zz <= 0) {
        clearInterval(t);
        loading = false;
      }
    }, 150);
  }

  var highlight = -1;
  for (var i = 0; i < section_list.length; i++) {
    var target = Math.max(0, document.getElementById(section_list[i]).offsetTop-document.getElementById("navbar").offsetHeight);
    if (curY+1 >= target) {
      highlight = i;
    }
  }
  var moveXN = $(window).width() * 0.18 - (curY - $('#diy').offset().top + 400) / $('#diy').height() * $(window).width()/2.3;
  var moveCB = $(window).width() * 0.22 - (curY - $('#diy').offset().top + 400) / $('#diy').height() * $(window).width()/2.3;
  if ($(window).scrollTop() > $('#diy').offset().top - 400) {
    $('#xn').css('left', moveXN + "px");
    $('#cb').css('right', moveCB + "px");
  }
}
